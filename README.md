# Thesis
1. How to run the project after cloning

```
cd repo
python -m venv venv

# On Linux/MacOS
source venv/bin/activate 

# On Windows
From command prompt
venv\Scripts\activate 

From power shell
venv\Scripts\Activate.ps1 

pip install -r requirements.txt

python manage.py runserver

```

