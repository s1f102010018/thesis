from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.views import LoginView, PasswordResetView, PasswordChangeView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views import View
from django.urls import reverse_lazy

from .forms import IDict, Profile, RegisterForm, LoginForm, UpdateUserForm, UpdateProfileForm
import openai
import json

openai.api_key = 'J2A1SE9tq3NSlX_CpODTK2Iwcm5d2x_XK-GCKsk-nNnV7kARI5IoVFDlFxVwymdV0gsbG8TNxTkLMVmmPndv0xQ'
openai.api_base = 'https://api.openai.iniad.org/api/v1'


def get_completion(prompt, model="gpt-3.5-turbo",temperature=0):
    messages = [{"role": "user", "content": prompt}]
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=temperature, # this is the degree of randomness of the model's output
    )
    return response.choices[0].message["content"]
def get_response(sentence, word, number_of_meanings=1, response_language="English"):
    prompt = f"""
        You are an intelligent dictionary.
        Given a sentence and a word in that sentence, you need to:

        1. Identify the language of the sentence.
        2. Provide {number_of_meanings} closest meanings in {response_language} to the meaning of the word in the sentence, along with their similarity scores.
        Include sample sentences in the identified language and their corresponding meanings in {response_language}.
        Ensure that the returned meanings are in {response_language}, and limit the sample sentences to a maximum of 10 words.

        3. Sort the meanings in descending order based on the similarity score.
        4. Format your response in JSON as follows:

        {{
            "Language": "",
            "Meanings": [
                {{
                    "meaning": "",
                    "similarity_score": "",
                    "example_sentence": "",
                    "example_sentence_meaning": ""
                }},
                ...
            ]
        }}
        
        The sentence is enclosed within triple backticks, and the word is enclosed within triple backticks.
        Sentence: ```{sentence}```
        Word: ```{word}```
    """

    response = get_completion(prompt, temperature=0.1)
    # print(response)
    return json.loads(response)

def search(request):
    if request.method == 'POST':
        form = IDict(request.POST)
        # print(form)
        if form.is_valid():
            # Process the form data
            # Access the form fields using form.cleaned_data dictionary
            sentence = form.cleaned_data['sentence']
            word = form.cleaned_data['word']
            response_language = form.cleaned_data['response_language']
            number_of_meanings = form.cleaned_data['number_of_meanings']
            
            # print(word, sentence, number_of_meanings, response_language)
            
            response = get_response(sentence, word, number_of_meanings, response_language)
            meanings = response['Meanings'][:int(number_of_meanings)]
            # print(response, type(response))
            # Redirect to a success page or do further processing
            return render(request, 'search.html', {'form': form, 'response': response, 'meanings': meanings})
        else:
            print(form.errors.as_data())
    else:
        form = IDict()
    return render(request, 'search.html', {'form': form})

def index(request):
    return render(request, 'home.html')

def contact(request):
    return render(request, 'contact.html')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        print (form)
        if form.is_valid():
            user = form.save()
            # Create UserProfile for the newly registered user
            Profile.objects.create(user=user)
            # Log the user in
            login(request, user)
            return redirect('login')  # Redirect to the homepage or any other desired page
    else:
        form = UserCreationForm()
    return render(request, 'registration/register.html', {'form': form})

@login_required
def profile(request):
    if request.method == 'POST':
        user_form = UpdateUserForm(request.POST, instance=request.user)
        profile_form = UpdateProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Your profile is updated successfully')
            return redirect(to='profile')
    else:
        user_form = UpdateUserForm(instance=request.user)
        profile_form = UpdateProfileForm(instance=request.user.profile)
    return render(request, 'profile.html', {'user_form': user_form, 'profile_form': profile_form})

# @login_required
# def custom_login(request, *args, **kwargs):
#     # If user is already authenticated, redirect to their profile
#     print(1231231)
#     if request.user.is_authenticated:
#         return redirect('profile')

#     # If user is not authenticated, continue with the regular login view
#     return auth_login(request, *args, **kwargs)

class RegisterView(View):
    form_class = RegisterForm
    initial = {'key': 'value'}
    template_name = 'registration/register.html'

    def dispatch(self, request, *args, **kwargs):
        # will redirect to the home page if a user tries to access the register page while logged in
        if request.user.is_authenticated:
            return redirect(to='/')

        # else process dispatch as it otherwise normally would
        return super(RegisterView, self).dispatch(request, *args, **kwargs)
    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            form.save()

            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}')

            return redirect(to='login')

        return render(request, self.template_name, {'form': form})
    
class CustomLoginView(LoginView):
    form_class = LoginForm

    def form_valid(self, form):
        remember_me = form.cleaned_data.get('remember_me')

        if not remember_me:
            # set session expiry to 0 seconds. So it will automatically close the session after the browser is closed.
            self.request.session.set_expiry(0)

            # Set session as modified to force data updates/cookie to be saved.
            self.request.session.modified = True

        # else browser session will be as long as the session cookie time "SESSION_COOKIE_AGE" defined in settings.py
        return super(CustomLoginView, self).form_valid(form)
    
class ResetPasswordView(SuccessMessageMixin, PasswordResetView):
    template_name = 'registration/password_reset.html'
    email_template_name = 'registration/password_reset_email.html'
    subject_template_name = 'registration/password_reset_subject'
    success_message = "We've emailed you instructions for setting your password, " \
                      "if an account exists with the email you entered. You should receive them shortly." \
                      " If you don't receive an email, " \
                      "please make sure you've entered the address you registered with, and check your spam folder."
    success_url = reverse_lazy('login')

class ChangePasswordView(SuccessMessageMixin, PasswordChangeView):
    template_name = 'registration/change_password.html'
    success_message = "Successfully Changed Your Password"
    success_url = reverse_lazy('profile')